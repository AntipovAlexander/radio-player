package co.kramatorsk.online;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import co.kramatorsk.online.fcm.Config;
import co.kramatorsk.online.main.fragments.AboutFragment;
import co.kramatorsk.online.main.fragments.AdsFragment;
import co.kramatorsk.online.main.fragments.ChannelsFragment;
import co.kramatorsk.online.main.fragments.ExtraFragment;
import co.kramatorsk.online.main.fragments.channels.PushFragment;
import co.kramatorsk.online.network.API;
import co.kramatorsk.online.network.responses.GeoByIp;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static co.kramatorsk.online.network.API.getPhoneName;

public class RadioNavActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    displayView(0);
                    return true;
                case R.id.navigation_one:
                    displayView(1);
                    return true;
                case R.id.navigation_two:
                    displayView(2);
                    return true;
                case R.id.navigation_three:
                    displayView(3);
                    return true;
            }
            return false;
        }
    };

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_radio);
      //  BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
      //  navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    Toast.makeText(context, displayFirebaseRegId(), Toast.LENGTH_SHORT).show();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    openNotifScreen(intent);
                }
            }
        };
      //  FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
        displayFirebaseRegId();

        displayView(0);

        bOpenPushes = false;
        if (this.getIntent().getExtras() != null) {
            bOpenPushes = this.getIntent().getExtras().getBoolean("isPush", false);
        }

        if (bOpenPushes) {
            openNotifScreen(this.getIntent());
        }
    }

    private boolean bOpenPushes;
    //static fields from player
    public static  int currChannel = -1;
    public static String artistName = "", songName = "";
    public static String artistNameRock = "", songNameRock = "";
    public static String artistNameRelax = "", songNameRelax = "";

    private void regUser(final String regId){
        Call<GeoByIp> bodyCall = API.getApiRequestService().geoData("http://ip-api.com/json");
        bodyCall.enqueue(new Callback<GeoByIp>() {
            @Override
            public void onResponse(Call<GeoByIp> call, Response<GeoByIp> response) {
                if (response.body().status.equals("success")) {
                    Call<ResponseBody> callReg = API.getApiRequestService().createUser(getPhoneName(),
                            "android", regId, response.body().city, response.body().country);
                    callReg.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<GeoByIp> call, Throwable t) {

            }
        });
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        Log.d("Firebase", "Firebase reg id: " + regId);
        if (regId!=null){
            regUser(regId);
        }
        return regId;
    }


    public void openNotifScreen(Intent intent) {
        String message = intent.getStringExtra("message");
        String title = intent.getStringExtra("title");
        String imageUrl = intent.getStringExtra("image");
        PushFragment fragment = PushFragment.newInstance(title, message, imageUrl);
        replaceFragment(fragment);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
    }

    private int currItem = -1;

    public void displayView(int position) {
        Fragment fragment = null;
        if (currItem != position) {
            switch (position) {
                case 0:
                    fragment = new ChannelsFragment();
                    currItem = 0;
                    break;
                case 1:
                    fragment = new ExtraFragment();
                    currItem = 1;
                    break;
                case 2:
                    fragment = new AdsFragment();
                    currItem = 2;
                    break;
                case 3:
                    fragment = new AboutFragment();
                    currItem = 3;
                    break;
                default:
                    break;
            }
            if (fragment != null) {
                replaceFragmentWithoutBackStack(fragment);
            }

        }

    }

    public void replaceFragmentWithoutBackStack(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }
}

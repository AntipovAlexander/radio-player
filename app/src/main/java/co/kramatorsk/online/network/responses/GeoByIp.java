package co.kramatorsk.online.network.responses;

/**
 * Created by Administrator on 30.08.2017.
 */

public class GeoByIp {
  //  {"as":"AS42590 Telemost LLC",
    // "city":"Dnipro",
    // "country":"Ukraine",
    // "countryCode":"UA",
    // "isp":"Telemost LLC",
    // "lat":48.463,
    // "lon":35.039,
    // "org":"Telemost LLC",
    // "query":"193.200.32.118",
    // "region":"12",
    // "regionName":"Dnipropetrovska Oblast'",
    // "status":"success",
    // "timezone":"Europe/Kiev",
    // "zip":""}
    public String as;
    public String city;
    public String country;
    public String countryCode;
    public String isp;
    public String lat;
    public String lon;
    public String org;
    public String query;
    public String region;
    public String regionName;
    public String status;
    public String timezone;
    public String zip;

}

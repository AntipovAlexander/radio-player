package co.kramatorsk.online.network;

import co.kramatorsk.online.network.responses.GeoByIp;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiService {

    String HTTP_URL = "http://api.kramatorsk.online/api/v1/";

    //['name', 'network_id', 'network_user_id', 'email', 'picture']
    @FormUrlEncoded
    @POST("device")
    Call<ResponseBody> createUser(@Field("device_id") String device_id,
                                        @Field("type") String type,
                                        @Field("push_token") String push_token,
                                        @Field("city") String city,
                                        @Field("country") String country);

    @GET
    Call<GeoByIp> geoData(@Url String fileUrl);
}

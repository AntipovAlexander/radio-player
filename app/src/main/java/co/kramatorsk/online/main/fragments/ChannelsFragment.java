package co.kramatorsk.online.main.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.robohorse.pagerbullet.PagerBullet;

import co.kramatorsk.online.R;
import co.kramatorsk.online.main.fragments.channels.ChannelOneFragment;
import co.kramatorsk.library.radio.RadioListener;
import co.kramatorsk.library.radio.RadioManager;

import static co.kramatorsk.online.RadioNavActivity.artistName;
import static co.kramatorsk.online.RadioNavActivity.artistNameRelax;
import static co.kramatorsk.online.RadioNavActivity.artistNameRock;
import static co.kramatorsk.online.RadioNavActivity.currChannel;
import static co.kramatorsk.online.RadioNavActivity.songName;
import static co.kramatorsk.online.RadioNavActivity.songNameRelax;
import static co.kramatorsk.online.RadioNavActivity.songNameRock;


public class ChannelsFragment extends Fragment implements RadioListener {

    public ChannelsFragment() {
        // Required empty public constructor
    }

    private final String[] RADIO_URL =
            {"http://212.26.146.50:8000/kr.mp3",
                    "http://212.26.146.50:8000/rock.mp3",
                    "http://212.26.146.50:8000/relax.mp3"};

    private final String[] RADIO_TITLES = {"RADIO", "ROCK", "RELAX"};

    private RadioManager mRadioManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRadioManager = RadioManager.with(getActivity());
        mRadioManager.registerListener(this);
        mRadioManager.setLogging(true);
        Log.d("ChannelsFragment ", "onCreate");
    }


    int pageNumber = 0;

    private PagerBullet pager;
    private PagerAdapter pagerAdapter;
    private ImageView logo, about;
    private TextView btn1;
    private View line1;
    public TextView mTextViewControl, mTextViewControl2;
    private ImageView facebook, twitter, youtube, insta, share;


    private final int[] COLORS = new int[]{R.color.colorPrimary, R.color.tomato, R.color.jungle_green};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_channels, container, false);
        Log.d("ChannelsFragment ", "onCreateView");
        btn1 = (TextView) v.findViewById(R.id.btn1);
        logo = (ImageView) v.findViewById(R.id.logo);

        facebook = (ImageView) v.findViewById(R.id.facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/kramatorsk.online"));
                startActivity(browserIntent);
            }
        });
        twitter = (ImageView) v.findViewById(R.id.twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/RadioKramatorsk"));
                startActivity(browserIntent);
            }
        });
        youtube = (ImageView) v.findViewById(R.id.youtube);
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCez1o5V7kUxhiYmOyxjsLtg"));
                startActivity(browserIntent);
            }
        });
        insta = (ImageView) v.findViewById(R.id.insta);
        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/kramatorsk.online"));
                startActivity(browserIntent);
            }
        });
        share = (ImageView) v.findViewById(R.id.share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                share.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share));
                startActivity(Intent.createChooser(share, getResources().getString(R.string.app_name)));
            }
        });

        about = (ImageView) v.findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDiag();
            }
        });
        line1 = v.findViewById(R.id.line1);
        mTextViewControl = (TextView) v.findViewById(R.id.textviewControl);
        mTextViewControl2 = (TextView) v.findViewById(R.id.textviewControl2);

        pager = (PagerBullet) v.findViewById(R.id.pagerBullet);
        pager.setIndicatorTintColorScheme(getResources().getColor(R.color.colorPrimary), Color.WHITE);
        pagerAdapter = new MyFragmentPagerAdapter(getChildFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.getViewPager().setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected, position = " + position);
                pageNumber = position;
                if (position == 0) {
                    setLabels(artistName, songName);
                } else if (position == 1) {
                    setLabels(artistNameRock, songNameRock);
                } else if (position == 2) {
                    setLabels(artistNameRelax, songNameRelax);
                }
                if (currChannel == position) {
                    mButtonControlStart.setBackground(getResources().getDrawable(R.drawable.ic_pause));
                } else {
                    mButtonControlStart.setBackground(getResources().getDrawable(R.drawable.ic_play_button));
                }
                if (position == 0) {
                    btn1.setText(RADIO_TITLES[position]);
                    btn1.setTextColor(getResources().getColor(R.color.colorPrimary));
                    //   line1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    pager.setIndicatorTintColorScheme(getResources().getColor(R.color.colorPrimary), Color.WHITE);
                    logo.setImageDrawable(getResources().getDrawable(R.drawable.logo_blue2));
                    mTextViewControl.setTextColor(getResources().getColor(COLORS[position]));
                } else if (position == 1) {
                    btn1.setText(RADIO_TITLES[position]);
                    btn1.setTextColor(getResources().getColor(R.color.tomato));
                    //   line1.setBackgroundColor(getResources().getColor(R.color.tomato));
                    pager.setIndicatorTintColorScheme(getResources().getColor(R.color.tomato), Color.WHITE);
                    logo.setImageDrawable(getResources().getDrawable(R.drawable.logo_red2));
                    mTextViewControl.setTextColor(getResources().getColor(COLORS[position]));

                } else if (position == 2) {
                    btn1.setText(RADIO_TITLES[position]);
                    btn1.setTextColor(getResources().getColor(R.color.jungle_green));
                    //   line1.setBackgroundColor(getResources().getColor(R.color.jungle_green));
                    pager.setIndicatorTintColorScheme(getResources().getColor(R.color.jungle_green), Color.WHITE);
                    logo.setImageDrawable(getResources().getDrawable(R.drawable.logo_green2));
                    mTextViewControl.setTextColor(getResources().getColor(COLORS[position]));
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        initializeUI(v);

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("ChannelsFragment ", "onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("ChannelsFragment ", "onDetach");
    }

    private Button mButtonControlStart;

    public void initializeUI(View view) {
        mButtonControlStart = (Button) view.findViewById(R.id.buttonControlStart);
        mButtonControlStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mRadioManager.isPlaying()) {
                    currChannel = pageNumber;
                    mRadioManager.startRadio(RADIO_URL[pageNumber]);
                    mButtonControlStart.setBackground(getResources().getDrawable(R.drawable.ic_pause));
                } else {
                    if (pageNumber == currChannel) {
                        currChannel = -1;
                        mRadioManager.stopRadio();
                        mButtonControlStart.setBackground(getResources().getDrawable(R.drawable.ic_play_button));
                    } else {
                        currChannel = pageNumber;
                        mRadioManager.stopRadio();
                        mRadioManager.startRadio(RADIO_URL[pageNumber]);
                        mButtonControlStart.setBackground(getResources().getDrawable(R.drawable.ic_pause));
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("ChannelsFragment ", "onResume");
        mRadioManager.connect();
        if (currChannel != -1) {
            pageNumber = currChannel;
            pager.setCurrentItem(pageNumber);
            if (mRadioManager.isPlaying()) {
                if (currChannel == 0) {
                    setLabels(artistName, songName);
                } else if (currChannel == 1) {
                    setLabels(artistNameRock, songNameRock);
                } else if (currChannel == 2) {
                    setLabels(artistNameRelax, songNameRelax);
                }
            }
        }
    }

    @Override
    public void onRadioLoading() {
    }

    @Override
    public void onRadioConnected() {
    }

    @Override
    public void onRadioStarted() {
    }

    @Override
    public void onRadioStopped() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("ChannelsFragment ", "onDestroy");
        mRadioManager.disconnect();
    }

    @Override
    public void onMetaDataReceived(String s, String s1) {
        //TODO Check metadata values. Singer name, song name or whatever you have.
        if (s != null && s.equals("StreamTitle")) {
            Log.d("onMetaData " + s + ": ", s1);
            final String ss = s1;
            // here you check the value of getActivity() and break up if needed
            if (getActivity() == null)
                return;

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (ss.contains("-")) {
                        String[] arr = ss.split("-");
                        if (currChannel == 0) {
                            songName = arr[1];
                            artistName = arr[0];
                        } else if (currChannel == 1) {
                            songNameRock = arr[1];
                            artistNameRock = arr[0];
                        } else if (currChannel == 2) {
                            songNameRelax = arr[1];
                            artistNameRelax = arr[0];
                        }
                        setLabels(arr[0], arr[1]);
                        mRadioManager.updateNotification(arr[0], arr[1], co.kramatorsk.library.R.drawable.icon22, co.kramatorsk.library.R.drawable.icon22);
                    } else {
                        setLabels(ss, "");
                        mRadioManager.updateNotification(ss, " ", co.kramatorsk.library.R.drawable.icon22, co.kramatorsk.library.R.drawable.icon22);

                    }

                }
            });
        }
    }

    @Override
    public void onError() {

    }


    public void setLabels(String s, String s1) {
        mTextViewControl.setText(s);
        mTextViewControl2.setText(s1);
    }

    static final String TAG = "myLogs";
    static final int PAGE_COUNT = 3;

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ChannelOneFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

    }


    private void showDiag() {
        final View dialogView = View.inflate(getActivity(), R.layout.about_dialog, null);
        final Dialog dialog = new Dialog(getActivity(), R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        RelativeLayout root = (RelativeLayout) dialog.findViewById(R.id.root);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                revealShow(dialogView, false, dialog);
            }
        });

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                revealShow(dialogView, true, null);
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    revealShow(dialogView, false, dialog);
                    return true;
                }
                return false;
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }


    private void revealShow(View dialogView, boolean b, final Dialog dialog) {
        final View view = dialogView.findViewById(R.id.root);
        int w = view.getWidth();
        int h = view.getHeight();
        int endRadius = (int) Math.hypot(w, h);
        int xw = w / 2;
        if (b) {
            // Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view, w, 0, 0, endRadius);
            view.setVisibility(View.VISIBLE);
            // revealAnimator.setDuration(600);
            //  revealAnimator.start();
        } else {
//            Animator anim = ViewAnimationUtils.createCircularReveal(view, w, 0, endRadius, 0);
//            anim.addListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    super.onAnimationEnd(animation);
//                    dialog.dismiss();
//                    view.setVisibility(View.INVISIBLE);
//                }
//            });
//            anim.setDuration(400);
//            anim.start();
            dialog.dismiss();
            view.setVisibility(View.INVISIBLE);
        }
    }

}

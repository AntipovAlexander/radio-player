package co.kramatorsk.online.main.fragments.channels;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import co.kramatorsk.online.R;


public class PushFragment extends Fragment {

    static final String ARGUMENT_TITLE = "title";
    static final String ARGUMENT_DESCR = "body";
    static final String ARGUMENT_IMAGE = "image";

    private String title, body,  image;



    public static PushFragment newInstance(String title, String body, String image) {
        PushFragment pageFragment = new PushFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_TITLE, title);
        arguments.putString(ARGUMENT_DESCR, body);
        arguments.putString(ARGUMENT_IMAGE, image);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title = getArguments().getString(ARGUMENT_TITLE);
        body = getArguments().getString(ARGUMENT_DESCR);
        image = getArguments().getString(ARGUMENT_IMAGE);
    }

    private TextView titleTv, messageTv;
    private ImageView imageIv, close;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notification, null);


        titleTv = (TextView) v.findViewById(R.id.title);
        titleTv.setText(title);

        messageTv = (TextView) v.findViewById(R.id.message);
        messageTv.setText(body);

        imageIv = (ImageView) v.findViewById(R.id.image);
        close = (ImageView) v.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        if (image!=null&&!image.equals("")&&!image.equals("null")){
            Glide.with(this).load(image).into(imageIv);
        }


        return v;
    }



}
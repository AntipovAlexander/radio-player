package co.kramatorsk.online.main.fragments.channels;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import co.kramatorsk.online.R;


public class ChannelOneFragment extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";

    int pageNumber;
    private final int[] BGS = new int[]{R.drawable.radio_11, R.drawable.rock_11, R.drawable.relax_11};

    public static ChannelOneFragment newInstance(int page) {
        ChannelOneFragment pageFragment = new ChannelOneFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, null);

        ImageView bg_main = (ImageView) view.findViewById(R.id.bg_main);
        bg_main.setImageDrawable(getResources().getDrawable(BGS[pageNumber]));



        return view;
    }



}
package co.kramatorsk.online.app;

import android.app.Application;
import android.util.Log;

/**
 * Created by Administrator on 24.09.2017.
 */

public class App extends Application {

    /*  static reference to object instance  */
    private static App _instance;

    public App() {
        Log.d("App ", "application object has been created");
    }

    public static App getInstance() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
    }

}
